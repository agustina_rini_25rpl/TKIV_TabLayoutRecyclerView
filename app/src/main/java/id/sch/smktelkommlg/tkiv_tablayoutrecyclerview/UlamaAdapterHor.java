package id.sch.smktelkommlg.tkiv_tablayoutrecyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class UlamaAdapterHor extends RecyclerView.Adapter<UlamaAdapterHor.ViewHolder> {

    ArrayList<Ulama> ulamaListHor;
    Context konteksH;

    public UlamaAdapterHor(ArrayList<Ulama> ulamaListHor, Context konteksH) {
        this.ulamaListHor = ulamaListHor;
        this.konteksH = konteksH;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ulama_list_hor, parent, false);
        ViewHolder vhh = new ViewHolder(v);
        return vhh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Ulama ulama = ulamaListHor.get(position);
        holder.tvNama.setText(ulama.name);
        holder.ivFoto.setImageDrawable(ulama.pic);

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vw) {
                Toast.makeText(konteksH, "You clickH " + ulama.name, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        //if (ulamaListHor != null)
        return ulamaListHor.size();
        //return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivFoto;
        TextView tvNama;
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ivFoto = itemView.findViewById(R.id.imageView);
            tvNama = itemView.findViewById(R.id.textViewNama);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }

}
