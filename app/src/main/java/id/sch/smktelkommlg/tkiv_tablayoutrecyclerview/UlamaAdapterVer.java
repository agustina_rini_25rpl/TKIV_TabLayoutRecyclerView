package id.sch.smktelkommlg.tkiv_tablayoutrecyclerview;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class UlamaAdapterVer extends RecyclerView.Adapter<UlamaAdapterVer.ViewHolder> {
    ArrayList<Ulama> ulamaListVer;
    Context konteksV;

    public UlamaAdapterVer(ArrayList<Ulama> ulamaListVer, Context konteksV) {
        this.ulamaListVer = ulamaListVer;
        this.konteksV = konteksV;
    }

    @Override
    public UlamaAdapterVer.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View h = LayoutInflater.from(parent.getContext()).inflate(R.layout.ulama_list_ver, parent, false);
        UlamaAdapterVer.ViewHolder vhv = new UlamaAdapterVer.ViewHolder(h);
        return vhv;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(UlamaAdapterVer.ViewHolder holder, int position) {
        final Ulama ulama = ulamaListVer.get(position);
        holder.namaU.setText(ulama.name);
        holder.gambar.setBackground(ulama.pic);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(konteksV, "You clickedV " + ulama.name, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        //if (ulamaListVer != null)
        return ulamaListVer.size();
        //return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView gambar;
        TextView namaU;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            gambar = itemView.findViewById(R.id.nama);
            namaU = itemView.findViewById(R.id.foto);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }
}
