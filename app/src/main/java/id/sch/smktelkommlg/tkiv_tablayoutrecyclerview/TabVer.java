package id.sch.smktelkommlg.tkiv_tablayoutrecyclerview;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class TabVer extends Fragment {

    ArrayList<Ulama> mListV = new ArrayList<>();
    UlamaAdapterVer mAdapterV;
    RecyclerView recyclerViewV;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_ver, container, false);

        recyclerViewV = view.findViewById(R.id.recyclerViewVer);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerViewV.setLayoutManager(layoutManager);
        mAdapterV = new UlamaAdapterVer(mListV, getContext());
        recyclerViewV.setAdapter(mAdapterV);
        fillData();

        return view;
    }

    private void fillData() {
        Resources resources = getResources();
        String[] arNama = resources.getStringArray(R.array.ulama);
        TypedArray a = resources.obtainTypedArray(R.array.ulama_picture);
        Drawable[] arFoto = new Drawable[a.length()];
        for (int i = 0; i < arFoto.length; i++) {
            arFoto[i] = a.getDrawable(i);
        }
        a.recycle();
        for (int i = 0; i < arNama.length; i++) {
            mListV.add(new Ulama(arNama[i], arFoto[i]));
        }
        mAdapterV.notifyDataSetChanged();
    }

}
