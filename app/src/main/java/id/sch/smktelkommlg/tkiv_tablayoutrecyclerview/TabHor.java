package id.sch.smktelkommlg.tkiv_tablayoutrecyclerview;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class TabHor extends Fragment {

    ArrayList<Ulama> mListH = new ArrayList<>();
    UlamaAdapterHor mAdapterH;
    RecyclerView recyclerViewH;
    Context konteksH;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View viewHor = inflater.inflate(R.layout.tab_hor, container, false);

        recyclerViewH = viewHor.findViewById(R.id.recyclerViewHor);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        //LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewH.setLayoutManager(layoutManager);
        mAdapterH = new UlamaAdapterHor(mListH, konteksH); //XXXXXX
        recyclerViewH.setAdapter(mAdapterH);
        fillData();

        return viewHor;
    }

    private void fillData() {
        Resources resources = getResources();
        String[] arNama = resources.getStringArray(R.array.ulama);
        TypedArray a = resources.obtainTypedArray(R.array.ulama_picture);
        Drawable[] arFoto = new Drawable[a.length()];
        for (int i = 0; i < arFoto.length; i++) {
            arFoto[i] = a.getDrawable(i);
        }
        a.recycle();
        for (int i = 0; i < arNama.length; i++) {
            mListH.add(new Ulama(arNama[i], arFoto[i]));
        }
        mAdapterH.notifyDataSetChanged();
    }
}
